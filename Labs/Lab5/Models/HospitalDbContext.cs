﻿using System.Data.Entity;

namespace Lab5.Models
{
    public class HospitalDbContext : DbContext
    {
        public DbSet<Doctor> Doctors { get; set; }
    }
}
