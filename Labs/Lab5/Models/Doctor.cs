﻿using System.ComponentModel.DataAnnotations;

namespace Lab5.Models
{
    public class Doctor
    {
        public int Id { get; set; }

        [Display(Name = "Имя")]
        [Required]
        [MaxLength(100)]
        public string FirstName { get; set; }

        [Display(Name = "Фамилия")]
        [Required]
        [MaxLength(100)]
        public string LastName { get; set; }

        [Display(Name = "Специальность")]
        [Required]
        [MaxLength(100)]
        public string Specialty { get; set; }
    }
}
