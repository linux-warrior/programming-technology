﻿namespace Lab5.Migrations
{
    using Models;
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<HospitalDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(HospitalDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
            context.Doctors.AddOrUpdate(
                new Doctor { Id = 1, FirstName = "Грегори", LastName = "Хаус", Specialty = "Инфекционист" },
                new Doctor { Id = 2, FirstName = "Лиза", LastName = "Кадди", Specialty = "Эндокринолог" },
                new Doctor { Id = 3, FirstName = "Джеймс", LastName = "Уилсон", Specialty = "Онколог" },
                new Doctor { Id = 4, FirstName = "Эрик", LastName = "Форман", Specialty = "Невролог" },
                new Doctor { Id = 5, FirstName = "Роберт", LastName = "Чейз", Specialty = "Хирург" },
                new Doctor { Id = 6, FirstName = "Эллисон", LastName = "Кэмерон", Specialty = "Иммунолог" }
            );
        }
    }
}
