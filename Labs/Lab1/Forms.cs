﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Lab1.Forms
{
    public class ValidationError : ApplicationException
    {
        public ValidationError(string message) : base(message) { }

        public override string ToString()
        {
            return Message;
        }
    }

    public class Field
    {
        private TextBox dataText;
        private Label errorLabel;

        public Field(TextBox dataText, Label errorLabel)
        {
            this.dataText = dataText;
            this.errorLabel = errorLabel;
        }

        public double Clean()
        {
            try
            {
                return Double.Parse(dataText.Text);
            }
            catch (FormatException)
            {
                throw new ValidationError("Необходимо ввести число.");
            }
        }

        public void showError(string message)
        {
            errorLabel.Text = message;
        }
    }

    public class FieldsDictionary : Dictionary<string, Field> { }

    public class ValuesDictionary : Dictionary<string, double> { }

    public class ErrorsDictionary : Dictionary<string, ValidationError> { }

    public class Form
    {
        private FieldsDictionary fields;
        public ValuesDictionary Values { get; private set; }
        public ErrorsDictionary Errors { get; private set; }

        public Form(FieldsDictionary fields)
        {
            this.fields = fields;
            Values = new ValuesDictionary();
            Errors = new ErrorsDictionary();
        }

        public void Clean()
        {
            Values.Clear();
            Errors.Clear();

            foreach (var fieldItem in fields)
            {
                string fieldName = fieldItem.Key;
                Field field = fieldItem.Value;
                string message = "";

                try
                {
                    Values[fieldName] = field.Clean();
                }
                catch (ValidationError error)
                {
                    Errors[fieldName] = error;
                    message = error.ToString();
                }

                fields[fieldName].showError(message);
            }
        }

        public bool IsValid()
        {
            Clean();
            return Errors.Count() == 0;
        }
    }
}
