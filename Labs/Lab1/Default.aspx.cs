﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e) { }

        protected void Add_Click(object sender, EventArgs e)
        {
            Calculate((leftVal, rightVal) => leftVal + rightVal);
        }

        protected void Subtract_Click(object sender, EventArgs e)
        {
            Calculate((leftVal, rightVal) => leftVal - rightVal);
        }

        protected void Multiply_Click(object sender, EventArgs e)
        {
            Calculate((leftVal, rightVal) => leftVal * rightVal);
        }

        protected void Divide_Click(object sender, EventArgs e)
        {
            Calculate((leftVal, rightVal) => leftVal / rightVal);
        }

        protected void Calculate(Func<double, double, double> operation)
        {
            var form = new Forms.Form(new Forms.FieldsDictionary
                {
                    { "leftVal", new Forms.Field(leftValText, leftValErrorLabel) },
                    { "rightVal", new Forms.Field(rightValText, rightValErrorLabel) },
                }
            );
            string result = "";

            if (form.IsValid())
            {
                result = operation(form.Values["leftVal"], form.Values["rightVal"]).ToString();
            }
            resultLabel.Text = result;
        }
    }
}
