﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab2_2
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Add_Click(object sender, EventArgs e)
        {
            Calculate((leftVal, rightVal) => leftVal + rightVal);
        }

        protected void Subtract_Click(object sender, EventArgs e)
        {
            Calculate((leftVal, rightVal) => leftVal - rightVal);
        }

        protected void Multiply_Click(object sender, EventArgs e)
        {
            Calculate((leftVal, rightVal) => leftVal * rightVal);
        }

        protected void Divide_Click(object sender, EventArgs e)
        {
            Calculate((leftVal, rightVal) => leftVal / rightVal);
        }

        protected void Calculate(Func<double, double, double> operation)
        {
            string result = "";

            if (Page.IsValid)
            {
                double leftVal = Double.Parse(leftValText.Text);
                double rightVal = Double.Parse(rightValText.Text);
                result = operation(leftVal, rightVal).ToString();
            }
            resultLabel.Text = result;
        }

        protected void ValidateDouble(Object source, ServerValidateEventArgs e)
        {
            e.IsValid = false;

            try
            {
                Double.Parse(e.Value);
                e.IsValid = true;
            }
            catch (FormatException)
            {          
            }
        }
    }
}
