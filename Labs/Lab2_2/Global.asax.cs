﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.UI;

namespace Lab2_2
{
    public class Global : System.Web.HttpApplication
    {
        protected void Application_Start(object sender, EventArgs e)
        {
            string jQueryVersion = "1.8.0";

            ScriptManager.ScriptResourceMapping.AddDefinition("jquery", new ScriptResourceDefinition {
                Path = string.Format("~/scripts/jquery-{0}.min.js", jQueryVersion),
                DebugPath = string.Format("~/scripts/jquery-{0}.js", jQueryVersion),
                LoadSuccessExpression = "window.jQuery"
            });
        }
    }
}
