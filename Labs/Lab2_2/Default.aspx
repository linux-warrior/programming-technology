﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Lab2_2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="Default.css" />
    <title>Калькулятор</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Калькулятор</h1>
        <div class="form-row">
            <asp:TextBox runat="server" ID="leftValText" />
            <asp:CustomValidator runat="server" ID="leftValValidator" CssClass="label error" ControlToValidate="leftValText"
                ValidateEmptyText="true" ErrorMessage="Необходимо ввести число." OnServerValidate="ValidateDouble" />
        </div>
        <div class="form-row">
            <asp:Button runat="server" ID="addButton" Text="+" OnClick="Add_Click" />
            <asp:Button runat="server" ID="subtractButton" Text="-" OnClick="Subtract_Click" />
            <asp:Button runat="server" ID="multiplyButton" Text="*" OnClick="Multiply_Click" />
            <asp:Button runat="server" ID="divideButton" Text="/" OnClick="Divide_Click" />
        </div>
        <div class="form-row">
            <asp:TextBox runat="server" ID="rightValText" />
            <asp:CustomValidator runat="server" ID="rightValValidator" CssClass="label error" ControlToValidate="rightValText"
                ValidateEmptyText="true" ErrorMessage="Необходимо ввести число." OnServerValidate="ValidateDouble" />
        </div>
        <div class="form-row">
            =
            <asp:Label runat="server" ID="resultLabel" CssClass="result" />
        </div>
    </form>
</body>
</html>
