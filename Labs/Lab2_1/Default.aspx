﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Lab2_1.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link rel="stylesheet" href="Default.css" />
    <title>Врачи</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Врачи</h1>
        <asp:GridView runat="server" ID="gridView" CssClass="grid-view" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField DataField="FirstName" HeaderText="Имя" />
                <asp:BoundField DataField="LastName" HeaderText="Фамилия" />
                <asp:BoundField DataField="Specialty" HeaderText="Специальность" />
            </Columns>
        </asp:GridView>
    </form>
</body>
</html>
