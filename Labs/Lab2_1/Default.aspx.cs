﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab2_1
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var doctorsList = new List<Doctor>()
                {
                    new Doctor { FirstName="Грегори", LastName="Хаус", Specialty="Инфекционист" },
                    new Doctor { FirstName="Лиза", LastName="Кадди", Specialty="Эндокринолог" },
                    new Doctor { FirstName="Джеймс", LastName="Уилсон", Specialty="Онколог" },
                    new Doctor { FirstName="Эрик", LastName="Форман", Specialty="Невролог" },
                    new Doctor { FirstName="Роберт", LastName="Чейз", Specialty="Хирург" },
                    new Doctor { FirstName="Эллисон", LastName="Кэмерон", Specialty="Иммунолог" }
                };
                gridView.DataSource = doctorsList;
                gridView.DataBind();
            }
        }
    }

    class Doctor
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Specialty { get; set; }
    }
}
