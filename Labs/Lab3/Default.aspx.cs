﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab3
{
    public partial class Default : System.Web.UI.Page
    {
        private string connectionString = WebConfigurationManager
            .ConnectionStrings["Hospital"].ConnectionString;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var connection = new SqlConnection(connectionString);
                using (connection)
                {
                    connection.Open();
                    var doctorsCmd = new SqlCommand(
                        "SELECT Doctor.FirstName, Doctor.LastName, Specialty.Name as Specialty " +
                        "FROM Doctor LEFT JOIN Specialty ON (Doctor.SpecialtyId = Specialty.Id)",
                        connection
                    );
                    var doctorsAdapter = new SqlDataAdapter(doctorsCmd);
                    var doctorsData = new DataSet();
                    doctorsAdapter.Fill(doctorsData, "Doctor");
                    gridView.DataSource = doctorsData;
                    gridView.DataBind();
                }
            }
        }
    }
}
